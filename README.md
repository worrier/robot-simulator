## Platform used to make this application
This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Below you will find some information on how to perform common tasks.<br>
You can find the most recent version of this guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md)

## Distributed version control, source code management functionality and deployment
1. Source code on Bitbucket repository - https://bitbucket.org/worrier/robot-simulator/src/develop/
2. Code deployed using AWS S3 bucket - http://robot-simulator.s3-website-us-east-1.amazonaws.com 

## Kickstart project on local environment
1. Clone the above mentioned repository
2. ```npm install``` to install all required dependencies to run the project
3. ```npm start``` to start project on local server

## Test data to exercise an application
You have to add command(s) into the text box and hit enter to execute it. i.e.
1. Single command at a time but first command shold be PLACE as listed below,
  - PLACE 0,0,NORTH
  - MOVE
  - RIGHT
  - MOVE
  - REPORT

2. You can even play with multiple  SPACE seperated commands at a same time. For this as well, you have to use atleast first time PLACE command. i.e.
  - PLACE 1,1,NORTH MOVE MOVE LEFT MOVE REPORT (output: 0,3,WEST)

## Timer
1. Timer is configurable. In constant file just change the value of const variable and it will reflact in the project 
2. After each 5 seconds of entered successful command, robot goes to idle condition
3. After 5 seconds, each command will execute but alert notification will be displayed on terminal

## Output Termial
1. Errors - Red color
2. Reports - White color
3. Robot idle condition - Light yellow color
4. When robot goes to idle position after specific seconds of time, output termianl will also gets clear




