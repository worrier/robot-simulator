export const LEFT_SHIFT_DIRECTION = ['north', 'west', 'south', 'east'];

export const  RIGHT_SHIFT_DIRECTION = ['north', 'east', 'south', 'west'];

export const SQUARE_TABLE_LIMIT = 5;

export const TERMINAL_PREFIX = {
    error: 'alert',
    log: 'report',
    idle: 'idle'
}

export const COMMANDS_LIST = ["place", "move", "left", "right", "report"];

export const IDLE_TIME_LIMIT_SECONDS = 5;