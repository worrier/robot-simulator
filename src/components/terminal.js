import React from "react";
import { TERMINAL_PREFIX } from "../helpers/Constants";

const OutputTerminal = ({ list }) => {
  let elemtnsList = list.map((item, index) => {
    return (
      <p className={item.type} key={index}>
        <span>{TERMINAL_PREFIX[item.type]}> </span>
        {item.message}
      </p>
    );
  });
  return (
    <div id="output-list" className="output-list">
      <span>Output Terminal</span>
      {elemtnsList}
    </div>
  );
};

export default OutputTerminal;
