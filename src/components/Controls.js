import React from 'react';
import PropTypes from 'prop-types';

const Controls = ({
    handleCommands
}) => {
    return (
        <input
            type="text"
            id="commandPrompt"
            className="command-prompt"
            onKeyDown={handleCommands}
            placeholder="Type your commands here and hit the Enter..."
          />
    );

}

Controls.propTypes = {
    handleCommands: PropTypes.func.isRequired,
}

export default Controls;


