import React, { Component } from "react";
import PropTypes from "prop-types";

class Timer extends Component {
  constructor(props) {
    super(props);
    this.state = { time: {}, seconds: this.props.seconds };
    this.timer = 0;
  }

  secondsToTime(secs) {
    let seconds = Math.ceil(secs % (60 * 60 * 60));

    return { s: seconds };
  }

  componentDidMount() {
    this.setState({ time: this.secondsToTime(this.state.seconds) });
  }

  startTimer = () => {
    this.clearTimer();
    this.timer = setInterval(this.countDown, 1000);
  };

  countDown = () => {
    // Remove one second, set state so a re-render happens.
    let seconds = this.state.seconds - 1;
    this.setState({
      time: this.secondsToTime(seconds),
      seconds: seconds
    });

    // Check if we're at zero.
    if (seconds === 0) {
        this.clearTimer();
      this.props.makeRobotOffline();
    }
  };

  clearTimer = () => {
      clearInterval(this.timer);
      this.setState({})
  }

  render() {
    const { time } = this.state;
    return <div className="timer-wrapper">Time to go robot in idle condition is: <span className="counter">{time.s}</span> seconds</div>;
  }
}

Timer.propTypes = {
  seconds: PropTypes.number.isRequired,
  makeRobotOffline: PropTypes.func
};

export default Timer;
