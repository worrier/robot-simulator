import React from "react";
import PropTypes from "prop-types";

const Button = ({ handleBtnClick, btnName }) => {
  return <button onClick={handleBtnClick}>{btnName}</button>;
};

Button.propTypes = {
  handleBtnClick: PropTypes.func,
  btnName: PropTypes.string.isRequired
};
export default Button;
