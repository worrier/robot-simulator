import React, { Component } from "react";
import Controls from "./components/Controls";
import {
  LEFT_SHIFT_DIRECTION,
  RIGHT_SHIFT_DIRECTION,
  SQUARE_TABLE_LIMIT,
  COMMANDS_LIST,
  IDLE_TIME_LIMIT_SECONDS
} from "./helpers/Constants";
import OutputTerminal from "./components/terminal";
import Timer from "./components/Timer";
import "./App.css";

class App extends Component {
  constructor() {
    super();
    this.state = {
      x: 0,
      y: 0,
      facing: "NORTH",
      isPlaced: null,
      report: null,
      isRobotOffline: false,
      showTimer: false,
      outputList: []
    };
  }

  /** 
    * Get the robot position, validate and place robot accordingly
    * @param {array} placings - all required data to place robot
    */
  handleRobotPlacing = (placings = []) => {
    const x = +placings[0],
      y = +placings[1],
      facing = placings[2] && placings[2].toUpperCase();

    if (this._isValidPosition(x) && this._isValidPosition(y)) {
      if (this._isValidDirection(facing)) {
        this.setState(
          {
            x,
            y,
            facing,
            isPlaced: true
          },
          () => {
            // Robot is all set to take position
            this.changeRobotPosition();
          }
        );
      } else {
        this.setState({
          isPlaced: false
        });
        this.logList(
          "Invalid direction, Please choose correct the direction",
          "error"
        );
      }
    }
  };

  /**
   * Modify the robot position on a table
   */
  changeRobotPosition = () => {
    let robot = document.getElementById("robot");

    robot.style.bottom = `${this.state.y * (100 / SQUARE_TABLE_LIMIT)}%`;
    robot.style.left = `${this.state.x * (100 / SQUARE_TABLE_LIMIT)}%`;

    this.startCountdown();
  };

  /**
   * Rotate robot on either left or right
   * @param {boolean} isRight - Direction of rotation
   */
  handleLeftRIghtRotate = isRight => {
    const { facing } = this.state;

    this.setFacing(facing, isRight);
  };

  /**
   * Check rotation direction and set robot's facing direction
   * @param {string} facing - old direction of robot
   * @param {boolean} isRight - Rotation direction 
   */
  setFacing = (facing, isRight) => {
    const directionArray = isRight
        ? RIGHT_SHIFT_DIRECTION
        : LEFT_SHIFT_DIRECTION,
      facingIndex = directionArray.indexOf(facing.toLowerCase()),
      position =
        facingIndex === directionArray.length - 1 ? 0 : facingIndex + 1;

    if (facingIndex > -1) {
      this.setState(
        {
          facing: directionArray[position].toUpperCase()
        },
        () => {
          //After changing the orientation stat counter
          this.startCountdown();
        }
      );
    }
  };

  /** Move a robot to the corrosponding direction using coordinate */
  handleMove = () => {
    let { x, y, facing } = this.state;

    if (facing === "NORTH") {
      if (this._isValidPosition(y + 1)) y = y + 1;
    } else if (facing === "SOUTH") {
      if (this._isValidPosition(y - 1)) y = y - 1;
    } else if (facing === "EAST") {
      if (this._isValidPosition(x + 1)) x = x + 1;
    } else if (facing === "WEST") {
      if (this._isValidPosition(x - 1)) x = x - 1;
    }
    this.setState(
      {
        x,
        y
      },
      () => {
        this.changeRobotPosition();
      }
    );
  };

  /**
   * Set the robot position of place command
   * @param {string} name - Property name of position object
   * @param {string} value - Property value of position object 
   */
  setPositionData = (name, value) => {
    this.setState({
      [name]: value
    });
  };

  /**
   * Disiplay the current position of robot on a table 
   */
  handleReport = () => {
    const { x, y, facing } = this.state;

    this.startCountdown();
    this.logList(
      `Robot has been placed at X=${x} and Y=${y} position by facing towards ${facing}`,
      "log"
    );
  };

  /**
   * Get the commands entered by user make sequence of function execution
   * @param {object} event - input box event object
   */
  handleCommands = event => {
    if (event.keyCode === 13) {
      let commandsList = COMMANDS_LIST,
        userEnteredCommands = event.target.value,
        enteredCommands = userEnteredCommands.split(" "),
        cmdFunctionList = [];

      //Create dynamic function list of entered commands
      enteredCommands.forEach((cmd, index) => {
        const cmdIndex = commandsList.indexOf(cmd.trim().toLowerCase());
        if (cmdIndex > -1) {
          cmdFunctionList.push(`handle${commandsList[cmdIndex]}`);
        } else {
          const hasColon = cmd.split(",").length > 1;
          // check incorrect command has a blank space(for empty spaces) and colons(robot placing values)
          cmd.length &&
            !hasColon &&
            this.logList(
              `${cmd} was Invalid command, please try again.`,
              "error"
            );
        }
      });

      this.executeCommands(cmdFunctionList, enteredCommands);
    }
  };

  /**
   * Takes list of commands and execute each function in sequence
   * @param {array} cmdFunctionList - list of command functions
   * @param {array} enteredCommands - List of commands entered by user
   */
  executeCommands(cmdFunctionList, enteredCommands) {
    const timeOut = this.getTimeOutTime(enteredCommands);

    /*eslint-disable */
    let handleplace,
      handlemove,
      handleleft,
      handleright,
      handlereport = null;
    handleplace = currentIndex => {
      const positions =
        enteredCommands[currentIndex + 1] &&
        enteredCommands[currentIndex + 1].split(",");
      this.handleRobotPlacing(positions);
    };
    handlemove = () => {
      setTimeout(() => {
        this._isPlaced() && this.handleMove();
      }, timeOut);
    };
    handleleft = () => {
      setTimeout(() => {
        this._isPlaced() && this.handleLeftRIghtRotate(false);
      }, timeOut);
    };
    handleright = () => {
      setTimeout(() => {
        this._isPlaced() && this.handleLeftRIghtRotate(true);
      }, timeOut);
    };
    handlereport = () => {
      setTimeout(() => {
        this._isPlaced() && this.handleReport();
      }, timeOut);
    };

    //Execute each function of a command functions list
    cmdFunctionList.reduce((total, currentCmdFunc, currentIndex) => {
      if (currentCmdFunc) {
        //before each command execution check robot is idle
        currentIndex === 0 && this._isRobotIdle();

        //evaluate function by it's name i.e handle(place/left/right/report)
        let funcToExecute = eval(`${currentCmdFunc}`);
        funcToExecute(currentIndex);
      }
      return;
    }, 0);
    /*eslint-enable */
  }

  /**
   * Return the required timeout to execute multiple functions
   * @param {array} enteredCommands  list of entered commands
   */
  getTimeOutTime = enteredCommands => {
    if (enteredCommands.length === 1) {
      return 0;
    } else {
      return 1000;
    }
  };

  /**
   * After each provided time make robot offline and clear terminal
   */
  makeRobotOffline = () => {
    this.setState({ isRobotOffline: true, outputList: [] });
  };

  /**
   * Check robot has gone in offline mode or not
   */
  _isRobotIdle = () => {
    this.state.isRobotOffline &&
      this.logList(
        `Robot has not been moved since last ${IDLE_TIME_LIMIT_SECONDS} seconds.`,
        "idle"
      );
  };

  /**
     * Check if the supplied position is valid
     * param {number} value - The x or y coordinate for checking.
     * return {boolean} Is valid position
     */
  _isValidPosition = value => {
    if (value < SQUARE_TABLE_LIMIT && value >= 0) {
      return true;
    } else {
      this.logList(
        `Table's limit is ${SQUARE_TABLE_LIMIT}. Thus, X or Y can range between 0 to ${SQUARE_TABLE_LIMIT -
          1}, please try again!`,
        "error"
      );
    }
  };

  /**
     * Check if the supplied facing direction is valid
     * param {string} str - The direction of placing
     * return {boolean} Is valid direction
     */
  _isValidDirection = str => {
    return /(north|south|east|west)/gi.test(str);
  };

  /**
     * Check if the robot has been placed on table or not
     * return {boolean} Is placed on table
     */
  _isPlaced = () => {
    return this.state.isPlaced
      ? true
      : this.logList(
          "Robot has not been placed on the table. Please use PLACE command to begins with!",
          "error"
        );
  };

  /**
   * list of logs to show in the terminals
   * @param {string} message - Message to show 
   * @param {string} type - Type of log such as error or log
   */
  logList = (message, type) => {
    this.test(() => {
      this.setState({
        outputList: [...this.state.outputList, { message, type }]
      });
    });
  };

  test = (cb) => {
    cb();
  }

  /** */
  startCountdown = () => {
    this.setState(
      { showTimer: true, isRobotOffline: false, key: Math.random() },
      () => {
        this.child.startTimer();
      }
    );
  };

  returnRef = ref => {
    this.child = ref;
  };

  render() {
    const { isPlaced, facing, outputList, showTimer } = this.state;

    return (
      <div className="App">
        <div className="table">
          <div
            id="robot"
            className={`robot ${isPlaced
              ? "placed"
              : "remove"} ${facing.toLowerCase()}`}
          >
            This side up
          </div>
        </div>

        <div className="command-holder">
          {showTimer && (
            <Timer
              seconds={IDLE_TIME_LIMIT_SECONDS}
              makeRobotOffline={this.makeRobotOffline}
              ref={this.returnRef}
              key={this.state.key}
            />
          )}
          <Controls handleCommands={this.handleCommands} />
          <OutputTerminal list={outputList} />
        </div>
      </div>
    );
  }
}

export default App;
